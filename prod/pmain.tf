module "web" {
  source = "../modules/"

  web_ami       = "${var.web_ami}"
  instance_type = "${var.instance_type}"
  key_name      = "${var.key_name}"
}

module "task-alarm" {
  source              = "../modules/aws-config/"
  alarm_name          = "${var.alarm_name}"
  comparison_operator = "${var.comparison_operator}"
  evaluation_periods  = "${var.evaluation_periods}"
  metric_name         = "${var.metric_name }"
  namespace           = "${var.namespace }"
  period              = "${var.period }"
  statistic           = "${var.statistic }"
  threshold           = "${var.threshold }"
  alarm_description   = "${var.alarm_description }"
  alarm_actions       = ["${var.alarm_actions}"]
  InstanceId          = "${module.task-alarm.web}"
}
