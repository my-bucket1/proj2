resource "aws_sns_topic" "SNS" {
  name = "${var.SNS_name}"
}

resource "aws_sns_topic_subscription" "user_updates_sns_target" {
  count = "${length("${var.subscriptions}")}"

  topic_arn = "${aws_sns_topic.SNS.id}"

  protocol = "${var.protocol }"

  endpoint = "${element("${var.subscriptions}", count.index)}"
}
